package com.olkhoviy.passive.exception;

public class EmptyOrderException extends Exception {
    @Override
    public void printStackTrace() {
        System.out.println("Nothing in your order :(");
    }
}
