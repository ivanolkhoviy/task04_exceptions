package com.olkhoviy.passive.exception;

public class NoSeatException extends  RuntimeException {
    @Override
    public void printStackTrace() {
        System.out.println("OOOPS there are no seats for you");
    }
}
