package com.olkhoviy.passive.controller;

import com.olkhoviy.passive.exception.EmptyOrderException;
import com.olkhoviy.passive.exception.NoSeatException;
import com.olkhoviy.passive.model.Kitchen;
import com.olkhoviy.passive.model.Order;
import com.olkhoviy.passive.view.PayDesk;


public class DiningRoom implements AutoCloseable {


    public static void takeASeat  () throws NoSeatException {
            int check = (int) (Math.random() * 30);
            if (check == 30) {
                try {
                    NoSeatException ex = new NoSeatException();
                    throw ex;

                } catch (NoSeatException e) {
                    e.printStackTrace();
                }
            } else System.out.println("You took a seat");


    }

    public static void makeAOrder(Order order){
        System.out.println("Your order is " + order.getDish());
    }

    public static void askForBill(Order order) throws EmptyOrderException {
        System.out.println("You have to pay " + PayDesk.countPrice(order) + " $");
    }
    public static void checkDishStatus(Order order){
        Kitchen.prepareFood(order);
    }

    public void close() throws Exception {
        EmptyOrderException e = new EmptyOrderException();
        try {
            throw e;
        }catch (EmptyOrderException eO){
            eO.printStackTrace();
        }
    }

}
