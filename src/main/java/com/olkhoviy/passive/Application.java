package com.olkhoviy.passive;

import com.olkhoviy.passive.controller.DiningRoom;
import com.olkhoviy.passive.exception.EmptyOrderException;
import com.olkhoviy.passive.exception.NoSeatException;
import com.olkhoviy.passive.model.Order;

public class Application {
    public static void main(String[] args) throws NoSeatException, EmptyOrderException {
        DiningRoom.takeASeat();
        Order order = new Order("Chicken with rice");
        DiningRoom.makeAOrder(order);
        DiningRoom.askForBill(order);

    }
}
